require s7plc
require modbus
require calc

epicsEnvSet("TOP", "../cwm-cws03_ctrl-plc-01")
cd $(TOP)

iocshLoad("iocsh/cwm_cws03_ctrl_plc_01.iocsh", "IPADDR=rfq-cooling-plc.tn.esss.lu.se, RECVTIMEOUT=2000, cwm-cws03_ctrl-plc-01_VERSION=plcfactory")
